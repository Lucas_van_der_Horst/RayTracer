# RayTracer
 
This is my raytracer project, it takes a virtual photo of a virtual scene by tracing a lightray for each pixel.

It's really inefficiënt, it uses one CPU core and I made this before I knew how to properly use Numpy.

## Some examples

One of the first results:

![Static](examples/static.png)

Use of self made photosphere:

![Use of a photosphere](examples/first_photosphere.png)

Another photosphere demo:

![Another in a grassy field](examples/grassy_field.png)

Took a couple hours to render this gif:

![A gif](examples/example_gif.gif)
