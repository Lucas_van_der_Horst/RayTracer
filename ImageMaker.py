import json

import numpy
from PIL import Image
import datetime
from pathlib import Path
import os

from MathStuff import *
from VisibleObjects import *


class Camera(object):
    def __init__(self, location, resolution, angles, sensitivity=None, screen_distance=1.5):
        if sensitivity is None:
            sensitivity = [[0, 255]] * 3
        self.location = location
        self.resolution = resolution
        self.sensitivity = sensitivity
        self.screen_distance = screen_distance
        self.angles = angles

    # TODO make this parallel
    def thread_pixel(self, pixels, sx, sy, visible_objects, light_sources, background, reflection_amount, thread_number,
                     total_threads):
        direction = Vector([self.screen_distance,
                            sy / self.resolution[1] - 0.5,
                            -(sx / self.resolution[0] - 0.5)])
        direction = direction.rotate(self.angles)
        ray = Ray(Vector(self.location), direction)

        light = ray.get_light(visible_objects, light_sources, background, ref_todo=reflection_amount)
        color = [(light[i] - self.sensitivity[i][0]) / (self.sensitivity[i][1] - self.sensitivity[i][0]) * 255
                 for i in range(len(light))]
        color = [0 if color[i] < 0 else color[i] if color[i] < 255 else 255 for i in range(len(color))]
        if thread_number % 100 == 0:
            print(f'{math.floor(thread_number / total_threads * 100)}% done')
        pixels[sx, sy] = color

    def take_picture(self, visible_objects, light_sources, background, reflection_amount=0):
        """Just for 3 dimensions to 2d picture"""
        pixels = numpy.zeros((self.resolution[0], self.resolution[1], 3), dtype=numpy.uint8)

        total_threads = self.resolution[0] * self.resolution[1]
        thread_number = 0
        for sx in range(self.resolution[0]):
            for sy in range(self.resolution[1]):
                self.thread_pixel(pixels, sx, sy, visible_objects, light_sources, background, reflection_amount,
                                  thread_number, total_threads)
                thread_number += 1

        return pixels


class Scene(object):
    visible_objects = []
    light_sources = []
    background = Background()
    camera = None

    def __init__(self):
        visible_objects = [
            Sphere(Vector([40, 0, 0]), 10, material=Material(test_color_b=[255, 255, 255])),
            Sphere(Vector([30, 10, 5]), 4, material=Material(test_color_b=[255, 255, 255])),
            Plane(-10, material=Material(0.3, 0.7))
        ]
        light_sources = [
            LightSource(Vector([0, 0, 100]), [20000, 20000, 20000])
        ]
        background = Background(image='space.jpg')
        camera = Camera(Vector([-5, -3, 5]), (1000, 1000), [0.0, 0.0, 0.2], screen_distance=1.5,
                        sensitivity=[[0, 255]] * 3)

    def serialize(self, obj):
        if type(obj) in (str, int, float, bool) or obj is None:
            pass
        elif type(obj) is list:
            return [self.serialize(i) for i in obj]
        elif type(obj) is tuple:
            return tuple([self.serialize(i) for i in obj])
        elif type(obj) is set:
            return set([self.serialize(i) for i in tuple(obj)])
        elif type(obj) is dict:
            for key in obj:
                obj[key] = self.serialize(obj[key])
        else:
            # obj is a custom class object
            return self.serialize({'class': obj.__class__.__name__, **vars(obj)})
        return obj

    def save(self, filename="scene.txt"):
        data = {
            'visible_objects': self.serialize(self.visible_objects),
            'light_sources': self.serialize(self.light_sources),
            'background': self.serialize(self.background),
            'camera': self.serialize(self.camera),
        }
        with open(filename, 'w') as file:
            file.write(json.dumps(data, indent=4))

    def load(self, filename):
        data = {}
        with open(filename, 'r') as file:
            data = json.loads(file.read())
        print(data)
        #self.visible_objects = scene_data.visible_objects
        #self.light_sources = scene_data.light_sources
        #self.background = scene_data.background
        #self.camera = scene_data.camera


def create_picture(camera, visible_objects, light_sources, background, file_name=None):
    image = Image.fromarray(camera.take_picture(visible_objects, light_sources, background, 10))
    if image.size[0] * image.size[1] <= 40000:
        image = image.resize((500, 500))
    image.show()
    if file_name is None:
        file_name = f'pictures/{str(datetime.datetime.now())}.png'
    image.save(file_name)
    print(f'Saved as {file_name}')
    return image


def record(camera, visible_objects, light_sources, background):
    directory = f"videos/{str(datetime.datetime.now()).replace(' ', '')}"
    Path(directory).mkdir(parents=True, exist_ok=True)
    clip_length = 80
    frames = []
    for i in range(clip_length):
        file_name = f"{directory}/image{'0'*(len(str(clip_length)) - len(str(i))) + str(i)}.png"
        frames.append(create_picture(camera, visible_objects, light_sources, background, file_name))
        camera.location[1] += 0.5
        print(f'{i / clip_length * 100}% done')

    frames[0].save(directory + '/gif.gif', save_all=True, append_images=frames[1:], optimize=False, duration=40, loop=0)
    # os.system(f"ffmpeg -f image2 -r 29 -i {directory}/image%0{str(len(str(clip_length)))}d.png -vcodec mpeg4 -y {
    # directory}/movie.mp4")


if __name__ == '__main__':
    test_scene = Scene()
    g_visible_objects = [
        Sphere(Vector([0, 40, 0]), 10, material=Material(0, 1, 0, test_color_b=[255, 255, 255])),
        Sphere(Vector([10, 30, 5]), 4, material=Material(0, 0.2, 0.8, test_color_b=[255, 255, 255])),
        Plane(-10, material=Material(0.5, 0.5, 0))
    ]
    g_light_sources = [
        LightSource(Vector([50, -20, 100]), [0, 0, 20000]),
        LightSource(Vector([-50, 20, 100]), [20000, 0, 0]),
        LightSource(Vector([100, 10, 100]), [0, 20000, 0]),
    ]
    g_background = Background()
    g_camera = Camera(Vector([11, 0, 10]), (200, 200), [-0.2, 0.1, 0.2], screen_distance=1.5,
                      sensitivity=[[0, 255]] * 3)

    test_scene.visible_objects = g_visible_objects
    test_scene.light_sources = g_light_sources
    test_scene.background = g_background
    test_scene.camera = g_camera

    test_scene.save('scene.txt')
    test_scene.load('scene.txt')

    # create_picture(g_camera, g_visible_objects, g_light_sources, g_background)
    # record(g_camera, g_visible_objects, g_light_sources, g_background)
