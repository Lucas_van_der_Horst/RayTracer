import math
from operator import mul
from matplotlib.image import imread


def distance_points(point1, point2=None) -> float:
    if point2 is None:
        point2 = [0] * len(point1)
    distance_squared = 0
    for i in range(len(point1)):
        distance_squared += math.pow(point1[i] - point2[i], 2)
    return math.sqrt(distance_squared)


def normalize_vector(vector) -> list:
    """Returns a vector where the length is 1"""
    factor = math.sqrt(sum([math.pow(vector[i], 2) for i in range(len(vector))]))
    return Vector([vector[i] / factor for i in range(len(vector))])


def add_lights(lights) -> list:
    """When multiple light beams hit the same spot, returns light"""
    return [sum([l[c] for l in lights]) for c in range(3)]


def light_on_surface(light, color, angle_difference) -> list:
    """Caculate the light that comes of when a light beam hits a surface, returns light"""
    return [light[i] * angle_difference * (color[i] / 255) for i in range(len(light))]


def light_over_distance(light, distance):
    return Vector(light) * (1 / distance)


def point_in_shadow(shadow_ray, visible_objects) -> bool:
    obstacles = []
    for visible_object in visible_objects:
        obstacles.extend(visible_object.ray_intersection(shadow_ray, max_t=1))
    return len(obstacles) > 0


def angle_vectors(vector1, vector2):
    return math.acos((vector1 * vector2) / (distance_points(vector1) * distance_points(vector2)))


def vector_difference(vector1, vector2) -> float:
    """Returns a float between 0 and 1, 1 is no difference, 0 is a 90 degree angle"""
    return 1 - angle_vectors(vector1, vector2) / (math.pi / 2)


def reflect_vector(beam, perpendicular):
    n = normalize_vector(perpendicular)
    return beam - (2 * (beam * n)) * n


class Vector(list):
    def __init__(self, values=None):
        if values is None:
            location = []
        super().__init__(values)
        self.values = list(self)

    def __setitem__(self, key, value):
        super().__setitem__(key, value)
        self.values = list(self)

    def __dir__(self):
        return ['values']

    def __sub__(self, other):
        return Vector([self[i] - other[i] for i in range(len(self))])

    def __mul__(self, other):
        if type(other) == Vector:
            return sum(map(mul, self, other))
        elif type(other) in (int, float):
            return Vector([self[i] * other for i in range(len(self))])
        else:
            raise ValueError(f'Multiply {type(self)} by unknown type {type(other)}')

    __rmul__ = __mul__

    def get_magnitude(self):
        return distance_points(self)

    def rotate(self, angles):
        """Only works in 3 dimensions"""
        # rotate around the X-axis
        vector = Vector(self)
        vector = [
            vector[0],
            vector[1] * math.cos(angles[0]) - vector[2] * math.sin(angles[0]),
            vector[1] * math.sin(angles[0]) + vector[2] * math.cos(angles[0]),
        ]

        # rotate around the Y-axis
        vector = [
            vector[0] * math.cos(angles[1]) + vector[2] * math.sin(angles[1]),
            vector[1],
            -vector[0] * math.sin(angles[1]) + vector[2] * math.cos(angles[1]),
        ]
        # rotate around the Z-axis
        vector = [
            vector[0] * math.cos(angles[2]) - vector[1] * math.sin(angles[2]),
            vector[0] * math.sin(angles[2]) + vector[1] * math.cos(angles[2]),
            vector[2],
        ]
        return Vector(vector)


class Ray(object):
    def __init__(self, start_point, direction):
        self.start_point = start_point
        self.direction = direction

    def t_to_point(self, t):
        point = Vector(self.start_point)
        for i in range(len(self.direction)):
            point[i] += self.direction[i] * t
        return point

    def closest_intersection(self, visible_objects):
        intersections = {}  # key = t, visible_object
        for visible_object in visible_objects:
            for intersection_t in visible_object.ray_intersection(self):
                intersections[intersection_t] = visible_object
        if len(intersections) > 0:
            closest_t = min(intersections.keys())
            return closest_t,  intersections[closest_t]
        else:
            return None, None

    def get_light(self, visible_objects, light_sources, background, ref_todo=0):
        """ref_todo is the amount of reflection still needed to be done"""
        closest_t, intersec_object = self.closest_intersection(visible_objects)
        if closest_t is None:
            return background.get_color(self)
        else:
            return intersec_object.render_light(closest_t, self, light_sources, visible_objects, background, ref_todo)


class Material(object):
    def __init__(self, reflectiveness=0.8, diffuseness=0.2, transparency=0.0,
                 test_color_a=(255, 255, 255), test_color_b=(100, 100, 100)):
        self.test_color_a = test_color_a
        self.test_color_b = test_color_b

        self.reflectiveness = reflectiveness
        self.diffuseness = diffuseness
        self.transparency = transparency

        # normalize
        combined = (self.reflectiveness, self.diffuseness, self.transparency)
        self.reflectiveness, self.diffuseness, self.transparency = [i * (1 / sum(combined)) for i in combined]

    def get_color(self, point):
        sum_point = sum([math.floor(i) for i in point])
        if sum_point % 2 == 0:
            return self.test_color_a
        else:
            return self.test_color_b


class VisibleObject(object):
    def __init__(self, material=Material()):
        self.material = material

    def ray_intersection(self, ray, min_t=0, max_t=math.inf):
        """Give t for where the ray intersects"""
        """Define per object"""
        return []

    def perpendicular_vector(self, col_point):
        """Give a vector perpendicular to the surface"""
        """Define per object"""
        return []

    def defused_light(self, light_sources, visible_objects, col_point, perpendicular, surface_color):
        lights = [[0, 0, 0]]
        for light_source in light_sources:
            shadow_ray = Ray(col_point, light_source.location - col_point)
            if not point_in_shadow(shadow_ray, visible_objects):    # TODO take transparency into account
                angle_diff = vector_difference(perpendicular, shadow_ray.direction)
                lights.append(light_on_surface(light_over_distance(light_source.get_light(),
                                                                   distance_points(col_point, light_source.location)),
                                               surface_color, angle_diff))
        return add_lights(lights)

    def reflection_light(self, light_sources, visible_objects, col_point, perpendicular, in_ray, background, ref_todo):
        if ref_todo > 0:
            reflection_ray = Ray(col_point, reflect_vector(in_ray.direction, perpendicular))
            reflection_ray: Ray
            return reflection_ray.get_light(visible_objects, light_sources, background, ref_todo - 1)
        return [0, 0, 0]

    def transparency_light(self, in_ray, in_point, perpendicular,
                           visible_objects, light_sources, background, ref_todo):
        inside_direction = in_ray.direction     # TODO bend ray inside
        inside_ray = Ray(in_point, inside_direction)
        closest_t = inside_ray.closest_intersection([self])[0]
        if closest_t is None:
            return inside_ray.get_light(visible_objects, light_sources, background, ref_todo-1)
        out_point = inside_ray.t_to_point(closest_t + 0.0000001)
        out_direction = inside_ray.direction       # TODO bend ray outside
        out_ray = Ray(out_point, out_direction)
        light = out_ray.get_light(visible_objects, light_sources, background, ref_todo-1)
        # TODO add light through color

        return light

    def render_light(self, intersection_t, in_ray, light_sources, visible_objects, background, ref_todo):
        in_ray: Ray
        outer_point = in_ray.t_to_point(intersection_t - 0.0000001)
        inner_point = in_ray.t_to_point(intersection_t + 0.0000001)
        perpendicular = self.perpendicular_vector(outer_point)
        surface_color = self.material.get_color(outer_point)

        lights = [
            self.defused_light(light_sources, visible_objects, outer_point, perpendicular, surface_color),
            self.reflection_light(light_sources, visible_objects, outer_point, perpendicular, in_ray, background, ref_todo),
            self.transparency_light(in_ray, inner_point, perpendicular,
                                    visible_objects, light_sources, background, ref_todo)
        ]
        # mix the light types according to the material properties
        lights = [
            Vector(lights[0]) * self.material.diffuseness,
            Vector(lights[1]) * self.material.reflectiveness,
            Vector(lights[2]) * self.material.transparency,
        ]

        return add_lights(lights)


class Background(object):
    def __init__(self, image=None, color=(0, 0, 0)):
        self.color = color
        if image is None:
            self.image = None
        else:
            self.image = imread('textures/'+image)

    def get_color(self, ray):
        """Only works for 3 dimensions"""
        ray: Ray
        direction = normalize_vector(ray.direction)
        if self.image is None:
            color = self.color
        else:
            horizontal_angle = (math.acos(direction[0] / math.sqrt(math.pow(direction[0], 2) + math.pow(direction[1], 2))))
            vertical_angle = (math.acos(direction[2] / 1))
            if direction[1] > 0:
                horizontal_angle *= -1
            horizontal_angle = horizontal_angle % (math.pi*2)
            x = math.floor((vertical_angle / math.pi) * len(self.image))
            y = math.floor((horizontal_angle / (math.pi * 2)) * len(self.image[0]))
            color = list(self.image[x, y])
        return color


class LightSource(object):
    def __init__(self, location, brightness):
        self.location = location
        self.brightness = brightness

    def get_light(self):
        return self.brightness
