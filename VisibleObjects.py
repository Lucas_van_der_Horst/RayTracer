from MathStuff import *


class Sphere(VisibleObject):
    def __init__(self, center, radius, material=Material()):
        super().__init__(material)
        self.center = center
        self.radius = radius

    def ray_intersection(self, ray, min_t=0, max_t=math.inf):
        """Only works for 3 dimensions at the moment"""
        ray: Ray

        a = math.pow(ray.direction[0], 2) + \
            math.pow(ray.direction[1], 2) + \
            math.pow(ray.direction[2], 2)
        b = 2 * ((ray.start_point[0] * ray.direction[0] - ray.direction[0] * self.center[0]) +
                 (ray.start_point[1] * ray.direction[1] - ray.direction[1] * self.center[1]) +
                 (ray.start_point[2] * ray.direction[2] - ray.direction[2] * self.center[2]))
        c = math.pow(ray.start_point[0], 2) - 2 * ray.start_point[0] * self.center[0] + math.pow(self.center[0],
                                                                                                   2) + \
            math.pow(ray.start_point[1], 2) - 2 * ray.start_point[1] * self.center[1] + math.pow(self.center[1],
                                                                                                   2) + \
            math.pow(ray.start_point[2], 2) - 2 * ray.start_point[2] * self.center[2] + math.pow(self.center[2],
                                                                                                   2) \
            - math.pow(self.radius, 2)
        d = math.pow(b, 2) - 4 * a * c

        if d < 0:
            return []
        t_list = [
            (-b - math.sqrt(d)) / (2 * a),
            (-b + math.sqrt(d)) / (2 * a),
        ]
        t_list = [t for t in t_list if max_t >= t >= min_t]
        if d == 0:
            return t_list[:1]
        else:
            return t_list

    def perpendicular_vector(self, point):
        return point - self.center


class Plane(VisibleObject):
    # TODO enable just a part of the plane and add rotation
    def __init__(self, height, material=Material()):
        super().__init__(material)
        self.height = height

    def ray_intersection(self, ray, min_t=0, max_t=math.inf):
        if ray.direction[2] == 0:
            return []
        t = (self.height - ray.start_point[2]) / ray.direction[2]
        if max_t > t > min_t:
            return [t]
        return []

    def perpendicular_vector(self, col_point):
        return Vector([0, 0, 1])